#@tool
#class_name rwTestResourceSaver
#extends ResourceFormatSaver
#
#
#func _get_recognized_extensions(_resource: Resource) -> PackedStringArray:
	#return PackedStringArray(["test"])
#
#
### Return true if this resource should be loaded as a DialogicCharacter
#func _recognize(resource: Resource) -> bool:
	#if resource is TestRes:
		#return true
	#return false
#
#
### Save the resource
#func _save(resource: Resource, path: String = '', _flags: int = 0) -> Error:
	#print("_save is called:", path, " resource:", resource)
	#ResourceSaver.save(resource as Resource, path) # is causing a recursive issue
	#return OK
