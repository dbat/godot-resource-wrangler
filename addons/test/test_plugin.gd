@tool
class_name TestPlugin
extends EditorPlugin

func _enter_tree() -> void:
	if Engine.is_editor_hint():
		pass


func _exit_tree() -> void:
	pass


func _has_main_screen() -> bool:
	return false


func _get_plugin_name() -> String:
	return "Test Plugin"

func _handles(object: Object) -> bool:
	return object is TestRes

func _edit(object: Object) -> void:
	print("Test Plugin _edit called with:", object)
