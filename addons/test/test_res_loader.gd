#@tool
#class_name rwTestResourceLoader
#extends ResourceFormatLoader
#
#
### Returns all accepted extenstions
#func _get_recognized_extensions() -> PackedStringArray:
	#return PackedStringArray(["test"])
#
#
### Returns "Rrsource" if this file can/should be loaded by this script
#func _get_resource_type(path: String) -> String:
	#var ext := path.get_extension().to_lower()
	#if ext == "test":
		#return "Resource"
#
	#return ""
#
#
### Return true if this type is handled
#func _handles_type(typename: StringName) -> bool:
	#return ClassDB.is_parent_class(typename, "Resource")
#
#
### Parse the file and return a resource
#func _load(path: String, _original_path: String, _use_sub_threads: bool, _cache_mode: int) -> Variant:
	#print("_load called:", path)
	#var r := ResourceLoader.load(path,"resource")
	#print(r)
	#return r
